import { Component } from '@angular/core';

@Component({
  selector: 'app-dynamic-field',
  templateUrl: './dynamic-field.component.html',
  styleUrls: ['./dynamic-field.component.css']
})
export class DynamicFieldComponent {
  userName = "";

  cleanUsername(){
    console.log("button clicked")
    this.userName = "";
  }
}