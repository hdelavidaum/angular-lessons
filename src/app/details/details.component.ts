import { Component } from "@angular/core";

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styles:[`
    .white-color {
      color: #fff;
    }
  `]
})
export class DetailsComponent {
  isVisible = false;
  timesClicked = [];
  
  onClick() {
    this.isVisible = !this.isVisible;
    this.timesClicked.push(new Date());
  }
}